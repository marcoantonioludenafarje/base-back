require('./env')
const http = require('http')
const mongoose = require('mongoose')
const { app } = require('./app/routes')
const { io } = require('./app/sockets')

// Set database
mongoose.connect(process.env.MONGODB_URI)
mongoose.Promise = global.Promise
const db = mongoose.connection
db.on('error', console.error)
db.once('open', initServer)

// Start
function initServer () {
  // Use db instance in app context
  app.context.db = db

  // Use socket instance in app context
  app.context.io = io

  // Server instance
  const server = http.Server(app.callback())

  // Attach websockets on server
  io.attach(server)

  const port = process.env.PORT || 3050
  const mode = process.env.NODE_ENV
  server.listen(port, () => console.log(`Listening on ${port} in ${mode}`))
}
