const dialogflow = require('dialogflow')
const projectId = process.env.DIALOGFLOW_PROJECT_ID
const languageCode = 'es'

module.exports = {
  processMessage,
  createEntityTypes,
  updateEntityType
}

// Process message
async function processMessage (text, sessionId) {
  const sessionClient = new dialogflow.SessionsClient()
  const session = sessionClient.sessionPath(projectId, sessionId)
  const req = {
    session,
    queryInput: {
      text: { text, languageCode }
    }
  }
  const res = await sessionClient.detectIntent(req)
  return res[0].queryResult
}

async function createEntityTypes (entityType) {
  const entityTypesClient = new dialogflow.EntityTypesClient()
  const intentsClient = new dialogflow.IntentsClient()
  const agentPath = intentsClient.projectAgentPath(projectId)
  const req = {
    parent: agentPath,
    entityType: {
      kind: 'KIND_MAP',
      autoExpansionMode: 'AUTO_EXPANSION_MODE_UNSPECIFIED',
      ...entityType
    }
  }
  const res = await entityTypesClient.createEntityType(req)
  return res[0]
}

async function updateEntityType (entityTypeId, entities) {
  const entityTypesClient = new dialogflow.EntityTypesClient()
  const entityTypePath = entityTypesClient.entityTypePath(projectId, entityTypeId)
  const getEntityTypeRequest = { name: entityTypePath }
  const [ entityType ] = await entityTypesClient.getEntityType(getEntityTypeRequest)
  entityType.entities = entities
  const request = { entityType }
  return entityTypesClient.updateEntityType(request)
}
