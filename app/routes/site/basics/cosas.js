const Router = require('koa-router')
const Trabajador = require('../../../models/basics/Trabajador')
const { processMessage } = require('../../../services/dialogflow')
const router = new Router()

router.get('/', async ctx => {
    // const trabajador = await Trabajador.find({published:true})
    // console.log(trabajador)
    // try{
    //   const result = await processMessage('hola', 'amazing-id')
    //   console.log("Se logro")
    //   console.log(result)
    //   ctx.body={message:'Se logro estoy en cosas '}
    // }catch(e){
    //   console.log("Hola aca veamos esto")
    //   ctx.body={message:'No Se logro'}
    // }
  const cosita = await Trabajador.find({})
  ctx.body=cosita
})

router.get('/:id', async ctx => {
  console.log(ctx.params)
  // const trabajador = await Trabajador.find({_id:ctx.params})
  const trabajador = await Trabajador.find({_id:ctx.params.id})
  console.log(trabajador)
  ctx.body=trabajador
})

// router.get('/:prueba', async ctx => {
//   const district = await District.findOne({ slug: ctx.params.slug })
//   ctx.assert(district, 404)
//   ctx.body = district
// })

// router.get('/:province', async ctx => {
//   const province = await Province.findOne({ slug: ctx.params.province })
//   const district = await District.find({ province: province._id })
//   ctx.assert(district, 404)
//   ctx.body = district
// })

module.exports = [
  router.routes(),
  router.allowedMethods()
]
