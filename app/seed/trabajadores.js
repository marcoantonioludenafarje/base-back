const Trabajador = require('../models/basics/Trabajador')

function createTrabajadores () {
  return Trabajador.create([
    {
      slug: 'marco-ludena',
      name: 'Marco Ludeña',
      cargo: 'Gerente General',
      published: true
    },
    {
      slug: 'marco-ludena 2',
      name: 'Marco Ludeña 2',
      cargo: 'Gerente General 2',
      published: true
    },
    {
      slug: 'marco-ludena 3',
      name: 'Marco Ludeña 3',
      cargo: 'Gerente General 3',
      published: true
    }
  ])
}

module.exports = { createTrabajadores }
