require('../../env')
const mongoose = require('mongoose')
const { createUsers } = require('./users')
const { createTrabajadores } = require('./trabajadores')


mongoose.connect(process.env.MONGODB_URI)
mongoose.Promise = global.Promise
var db = mongoose.connection
db.on('error', console.error)
db.once('open', run)

async function run () {
  try {
    await createUsers()
    console.log('Employees created')

    await createTrabajadores()
    console.log('Trabajadores creados')

  } catch (e) {
    console.log(e.message)
  } finally {
    db.close()
  }
}
