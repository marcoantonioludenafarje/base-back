  const User = require('../models/user/User')

async function createUsers () {
  const users = await User.create([
    { name: 'prueba', auth: { email: 'prueba@globalhitss.com', password: 'globalhitsspassword' } },
    { name: 'prueba-2', auth: { email: 'prueba-2@globalhitss.com', password: 'globalhitsspassword-2' } }
  ])
  return users.map(a => a._id)
}

module.exports = { createUsers }
