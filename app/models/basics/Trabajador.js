const mongoose = require('mongoose')
const { Schema } = mongoose

var trabajadorSchema = new Schema({
  slug: {
    type: String,
    required: true,
    regex: /^[a-z-\d]+$/,
    unique: true,
    index: true
  },
  name: {
    type: String,
    required: true,
    maxlength: 50
  },
  cargo: {
    type: String,
    required: true,
    maxlength: 50
  },
  published: {
    type: Boolean,
    default: false,
    required: true
  }
}, {
  timestamps: true,
  toJSON: { virtuals: true }
})

module.exports = mongoose.model('Trabajador', trabajadorSchema)
