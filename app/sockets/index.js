const socketio = require('socket.io')
const onUserAction = require('./user-action')
const onUserMessage = require('./user-message')
const mensajeUsuario = require('./mensajeUsuario')
var io = socketio()
var conexiones = 0
io.on('connection', socket => {
  // Listeners
  conexiones++
  console.log('Usuario conectado' + conexiones)
  // console.log(socket)
  socket.on('disconnect', () => {
    console.log('Usuario desconectado')
  })
  socket.on('enviarMensaje', mensajeUsuario)

  socket.on('user action', onUserAction)
  socket.on('user message', onUserMessage)
})

module.exports = { io }
