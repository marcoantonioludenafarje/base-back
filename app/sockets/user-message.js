const { processMessage } = require('../services/dialogflow')

module.exports = async function onUserMessage (text) {
  const socket = this

  // Build bot event
  const event = {
    author: 'bot'
  }

  try {
    console.log("Va a empezar el process Message")

    const result = await processMessage(text, socket.id)
    
    console.log("Acabo el process Message")
    console.log(JSON.stringify(result, 2, 2))
    event.messages = result.fulfillmentMessages
      .filter(m => m.platform === 'PLATFORM_UNSPECIFIED')
  } catch (e) {
    console.log(e.message)
    event.messages = [{
      text: {
        text: [
          'Ups, no he entendido a que te refieres. xdddddddddd'
        ]
      }
    }]
  }

  socket.emit('bot event', event)
}
