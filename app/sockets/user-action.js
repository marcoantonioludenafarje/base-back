module.exports = async function onUserAction (action) {
  const socket = this

  // Build bot event
  const event = {
    author: 'bot',
    messages: [{
      text: {
        text: [
          getMessage(action)
        ]
      }
    }]
  }

  // Action
  // event.action = {
  //   name: '',
  //   params: {}
  // }

  socket.emit('bot event', event)
}

function getMessage (action) {
  const actions = {
    'page-load': '¡Bienvenido!'
  }

  return actions[action] || 'Lo siento, no entendí.'
}
