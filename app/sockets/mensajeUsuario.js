const { processMessage } = require('../services/dialogflow')

module.exports = async function mensajeUsuario (text) {
  const socket = this
  const event = {
    author: 'bot'
  }
  try{
    console.log('Va a empezar el process Message')
    console.log(text)
    const result = await processMessage(text.mensaje,socket.id)
    event.messages = result.fulfillmentMessages
      .filter(m => m.platform === 'PLATFORM_UNSPECIFIED')
    // console.log(event[0].text)

  }catch (e) {
    console.log('Ocurrio un error en el dialogflow')
    console.log(e)
    // event.messages=[{
    //   text:{
    //     text:[
    //       'Ups , no entendi lo que necesitas'
    //     ]
    //   }
    // }]
  }
  console.log('Se imprime desde el servidor el mensaje')
  console.log(text)
  console.log("El id del chico es ")
  console.log(socket.id)
  socket.emit('bot event', event)
}
