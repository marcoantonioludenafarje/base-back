const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition
const Vue = window.Vue
var socket = io()

window.app = new Vue({
  el: '#app',
  data: {
    messages: [],
    iniciar: false
  },
  mounted () {
    socket.on('connect', function () {
      console.log('Conectado al servidor')
    })
    socket.on('bot event', m => {
      console.log("Aca viene la respuesta de mi server")
      console.log(m)
      // var principalMessage= m.messages[0].text.text[0]
      // console.log(principalMessage)
      // this.messages.push(principalMessage)
      // this.speak(principalMessage)


    })
    this.test()
  },
  methods: {
    async speak (message) {
      window.speechSynthesis.cancel()
      var msg = new window.SpeechSynthesisUtterance(message)
      msg.lang = 'es-PE'
      msg.rate = 1.20
      window.speechSynthesis.speak(msg)
    },
    test () {
      console.log("Entro a this test")
      var recognition = new SpeechRecognition()
      recognition.lang = 'es-ES'

      recognition.onresult = event => {
        console.log(event)
        var mensajeDevuelto=event.results[0][0].transcript
        var auxiliarMessage=mensajeDevuelto.toLowerCase()
        if (auxiliarMessage.slice(0, 5) == 'claro') {
          console.log('Realizar envio ')
          // this.messages.push(event.results[0][0].transcript)
          this.messages.push(mensajeDevuelto.slice(6, mensajeDevuelto.length))
          this.sendMessage(mensajeDevuelto.slice(6, mensajeDevuelto.length))
        } else {
          console.log('No realizar envio')
        }
      }
      //
      recognition.onspeechend = () => {
        console.log("Realizo una pausa")
        recognition.stop()
        this.test()
      }
      //
      recognition.onerror = event => {
        console.log("Un error")
        console.log(event.error)
        this.test()
      }

      recognition.start()
    },
    sendMessage (message) {
      socket.emit('enviarMensaje',{
        usuario:'Fernando',
        mensaje:message
      })
    }
  }
})
